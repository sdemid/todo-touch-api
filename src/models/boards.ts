import query from '@/service/TouchORM'
import {Id, ModelParams } from '../types'

export const createBoard = async (boardName?: string) => {
  const fields = {
    name: boardName,
    created_at: new Date().toISOString().
      replace(/T/, ' ').
      replace(/\..+/, ''),
  }

  await query('boards')
    .create(fields)
    .init()
}

export const getBoardsList: ModelParams = async ({
  filterBy,
  sort = { fields: ['id'], order: 'ASC' },
  pager = {
    offset: 0,
    limit: 100,
  },
}) => {
  const board = await query('boards')
    .select('*')
    .where(filterBy)
    .orderBy(sort.fields, sort.order)
    .limit(pager.limit, pager.offset)
    .init()

  return board
}

export const getBoard = async (
  params: { id?: Id, boardName?: string },
) => {
  const [board] = await query('boards')
    .select('*')
    .where(params)
    .init()

  return board
}

export const deleteBoard = async (id?: Id) => {
  await query('boards')
    .delete()
    .where({ id })
    .init()
}

export const updateBoard = async (
  id: Id, fields: { name?: string },
) => {
  await query('boards')
    .update(fields)
    .where({ id })
    .init()
}
