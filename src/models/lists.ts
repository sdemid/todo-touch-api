import query from '@/service/TouchORM'
import {Id, ModelParams } from '../types'

export const createList = async (listName: string, boardId: string) => {
  const fields = {
    board_id: boardId,
    name: listName,
    created_at: new Date().toISOString().
      replace(/T/, ' ').
      replace(/\..+/, ''),
  }

  await query('lists')
    .create(fields)
    .init()
}

export const getLists: ModelParams = async ({
  filterBy = {},
  sort = { fields: ['id'], order: 'ASC' },
  pager = {
    offset: 0,
    limit: 100,
  },
}) => {
  const board = await query('lists')
    .select('*')
    .where(filterBy)
    .orderBy(sort.fields, sort.order)
    .limit(pager.limit, pager.offset)
    .init()

  return board
}

export const getListItem = async (
  params: { id?: Id, name?: string },
) => {
  const [listItem] = await query('lists')
    .select('*')
    .where(params)
    .init()

  return listItem
}

export const updateListItem = async (
  id: Id, fields: { name?: string },
): Promise<void> => {
  await query('lists')
    .update(fields)
    .where({ id })
    .init()
}

export const deleteListItem = async (id?: Id): Promise<void> => {
  await query('lists')
    .delete()
    .where({ id })
    .init()
}
