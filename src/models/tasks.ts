import query from '@/service/TouchORM'
import {Id, ModelParams } from '../types'

type CreateTaskParams = (
  fields: {
    title: string,
    description?: string,
    listId: string,
  },
) => Promise<any>

export const createTask: CreateTaskParams = async ({
  title,
  description,
  listId,
})  => {
  const fields = {
    title,
    description,
    is_completed: false,
    created_at: new Date().toISOString(). // CURRENT_TIMESTAMP
      replace(/T/, ' ').
      replace(/\..+/, ''),
    lists_id: listId,
  }

  await query('tasks')
    .create(fields)
    .init()
}

export const getTasksList: ModelParams = async ({
  filterBy = {},
  sort = { fields: ['id'], order: 'ASC' },
  pager = {
    offset: 0,
    limit: 100,
  },
})  => {
  const board = await query('tasks')
    .select('*')
    .where(filterBy)
    .orderBy(sort.fields, sort.order)
    .limit(pager.limit, pager.offset)
    .init()

  return board
}

export const getTask = async (
  params: { id?: Id, name?: string },
) => {
  const [task] = await query('tasks')
    .select('*')
    .where(params)
    .init()

  return task
}

export const updateTask = async (
  id: Id, fields: { title?: string, description?: string, isCompleted?: boolean },
) => {
  const {
    title,
    description,
    isCompleted,
  } = fields

  await query('tasks')
    .update({
      title,
      description,
      is_completed: isCompleted,
    })
    .where({ id })
    .init()
}

export const deleteTask = async (id?: Id): Promise<void> => {
  await query('tasks')
    .delete()
    .where({ id })
    .init()
}
