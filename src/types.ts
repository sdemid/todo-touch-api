export type Id = string | number;

export type ModelParams = (
  params: {
    filterBy?: { id?: Id, boardName?: string },
    sort?: { fields: string[], order: string },
    pager?: { limit: number, offset: number },
  },
) => Promise<any>
